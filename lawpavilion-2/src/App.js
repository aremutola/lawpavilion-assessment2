import Navbar from './components/navbar';
import Content from './components/content';
import Footer from './components/footer';
import './App.css';

function App() {

  return (
    <div>
      <Navbar />
      <Content />
      <Footer />
    </div>
  );
}

export default App;