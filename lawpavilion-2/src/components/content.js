import React from 'react';
import './content.css';
import flag from '../assets/flag.webp';
import Ramsey from '../assets/1ramsey.webp';
import Davido from '../assets/2davido.webp';
import Destiny from '../assets/3etiko.webp';
import Odun from '../assets/4odun.webp';
import Osimhen from '../assets/5Osimhen.webp';
import Wizkid from '../assets/6wizkid.webp';
import Olamide from '../assets/7olamide.webp';
import Mercy from '../assets/8Mercy-Johnson.webp';
import Ahmed from '../assets/9ahmed.webp';
import Burna from '../assets/10burna.webp';

const celebs = [
  {avatar: Ramsey ,firstname:'Ramsey', lastname:'Noah', industry:'Nollywood', nationality: flag},
  {avatar: Davido ,firstname:'David', lastname:'Adeleke', industry:'Music', nationality: flag},
  {avatar: Destiny ,firstname:'Destiny', lastname:'Etiko', industry:'Nollywood', nationality: flag},
  {avatar: Odun ,firstname:'Odunlade', lastname:'Adekola', industry:'Nollywood', nationality: flag},
  {avatar: Osimhen ,firstname:'Victor', lastname:'Osimhen', industry:'Sports', nationality: flag},
  {avatar: Wizkid ,firstname:'Ayo', lastname:'Balogun', industry:'Music', nationality: flag},
  {avatar: Olamide ,firstname:'Olamide', lastname:'Adedeji', industry:'Music', nationality: flag},
  {avatar: Mercy ,firstname:'Mercy', lastname:'Johnson', industry:'Nollywood', nationality: flag},
  {avatar: Ahmed ,firstname:'Ahmed', lastname:'Musa', industry:'Sports', nationality: flag},
  {avatar: Burna ,firstname:'Damini', lastname:'Ogulu', industry:'Music', nationality: flag}
]

function Content() {
  return(
    <div className="contentcontainer">
      <div className="contentbag">
        {
          celebs.map((celeb) => {
              return(
                <div key={celeb.id} className="arraycontainer">
                  <div className="avatarcontainer"><img src={ celeb.avatar } alt="avatars" className="cardavatar" /></div>
                  <div className="cardflex">
                    <div className="carddescription">
                      <div className="cardwords">First Name:</div>
                      <div className="cardwords">Last Name:</div>
                      <div className="cardwords">Industry:</div>
                      <div className="cardwords">Nationality:</div>
                    </div>
                    <div className="cardbiodata">
                      <div className="cardwords">{ celeb.firstname }</div>
                      <div className="cardwords">{ celeb.lastname }</div>
                      <div className="cardwords">{ celeb.industry }</div>
                      <img src={ celeb.nationality } alt="nationality" className="flag cardwords" />
                    </div>
                  </div>
                  <div className="card-container">
                    <button className="card-button">Full Profile</button>
                  </div>
                </div>
              )
          })
        }
      </div>
    </div>
  )
}

export default Content;