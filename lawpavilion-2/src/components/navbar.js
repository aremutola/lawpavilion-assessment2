import React from 'react';
import './navbar.css'

function Navbar() {
  return(
    <nav className="navcontainer">
      <div className="navflex">
        <ul className="listflex">
          <a href="/">
            <li className="homeitem">Home</li>
          </a>
          <a href="/">
            <li>Gossips</li>
          </a>
        </ul>

        <ul className="navitem2">
          <a href="/">
            <li>Archive</li>
          </a>
        </ul>
      </div>
    </nav>
  )
}

export default Navbar;